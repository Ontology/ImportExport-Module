﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;

namespace ImportExport_Module
{
    public class ImportWorker
    {
        private Globals _globals;

        private clsDataTypes _dataTypes = new clsDataTypes();

        public OntologyModDBConnector _ontologyModDbConnector;

        private XmlTextReader _xmlTextReader;

        private List<clsOntologyItem> _oItems = new List<clsOntologyItem>();
        private List<clsOntologyItem> _classes = new List<clsOntologyItem>();
        private List<clsClassAtt> _classAttributes = new List<clsClassAtt>();
        private List<clsClassRel> _classRelations = new List<clsClassRel>();
        private List<clsObjectAtt> _objAtttributes = new List<clsObjectAtt>();
        private List<clsObjectRel> _objRelations = new List<clsObjectRel>();

        private void ClearLists()
        {
            _oItems.Clear();
            _classes.Clear();
            _classAttributes.Clear();
            _classRelations.Clear();
            _objAtttributes.Clear();
            _objRelations.Clear();
        }

        public clsOntologyItem ImportTemplates(Assembly assembly)
        {
            var result = _globals.LState_Success.Clone();

            ClearLists();

            foreach (var manifestInfo in assembly.GetManifestResourceNames())
            {
                if (manifestInfo.ToLower().EndsWith("Template.xml".ToLower()) ||
                    manifestInfo.ToLower().EndsWith(".xml".ToLower()))
                {
                    Stream xmlStream = assembly.GetManifestResourceStream(manifestInfo);

                    result = AddItemsFromXMLStream(xmlStream);
                    xmlStream.Close();
                }
            }

            if (result.GUID == _globals.LState_Success.GUID)
            {
                result = SaveItems();
            }
            return result;
        }

        public clsOntologyItem ImportXMLFiles(string path)
        {
            var result = _globals.LState_Success.Clone();

            ClearLists();

            var fileList = Directory.GetFiles(path).Where(file => Path.GetExtension(file).ToLower() == ".xml").ToList();

            foreach (var file in fileList)
            {
                Stream xmlStream = new FileStream(file, FileMode.Open);
                result = AddItemsFromXMLStream(xmlStream);
                xmlStream.Close();

                if (result.GUID == _globals.LState_Success.GUID)
                {
                    result = SaveItems();
                }

                xmlStream.Close();
            }

            return result;
        }

        private clsOntologyItem SaveItems()
        {
            var result = _globals.LState_Success.Clone();

            if (_classes.Any())
            {
                result = FileItems(_classes);
            }

            if (_oItems.Any())
            {
                result = _ontologyModDbConnector.SaveObjects(_oItems);
            }

            if (_classAttributes.Any())
            {
                result = _ontologyModDbConnector.SaveClassAtt(_classAttributes);
            }

            if (_classRelations.Any())
            {
                result = _ontologyModDbConnector.SaveClassRel(_classRelations);
            }

            if (_objAtttributes.Any())
            {
                result = _ontologyModDbConnector.SaveObjAtt(_objAtttributes);
            }

            if (_objRelations.Any())
            {
                result = _ontologyModDbConnector.SaveObjRel(_objRelations);
            }

            return result;

        }

        private clsOntologyItem AddItemsFromXMLStream(Stream xmlStream)
        {
            string type;
            var result = _globals.LState_Success.Clone();

            using (_xmlTextReader = new XmlTextReader(xmlStream))
            {
                _xmlTextReader.ReadToDescendant(XMLEntries.Items);
                if (_xmlTextReader.LocalName == XMLEntries.Items)
                {
                    type = _xmlTextReader.GetAttribute(XMLEntries.Type);
                    if (!string.IsNullOrEmpty(type))
                    {
                        while (_xmlTextReader.ReadToFollowing(XMLEntries.Item))
                        {
                            if (type == _globals.Type_AttributeType)
                            {
                                var oItem_Item = GetSimpleItemFromXmlData(type);

                                result = _ontologyModDbConnector.SaveAttributeTypes(new List<clsOntologyItem> { oItem_Item });
                                if (result.GUID == _globals.LState_Error.GUID)
                                {
                                    break;
                                }
                            }
                            else if (type == _globals.Type_Class)
                            {
                                var oItem_Item = GetSimpleItemFromXmlData(type);

                                _classes.Add(oItem_Item);
                            }
                            else if (type == _globals.Type_Object)
                            {
                                var oItem_Item = GetSimpleItemFromXmlData(type);

                                _oItems.Add(oItem_Item);
                            }
                            else if (type == _globals.Type_RelationType)
                            {
                                var oItem_Item = GetSimpleItemFromXmlData(type);

                                result = _ontologyModDbConnector.SaveRelationTypes(new List<clsOntologyItem> { oItem_Item });
                                if (result.GUID == _globals.LState_Error.GUID)
                                {
                                    break;
                                }

                            }
                            else if (type == _globals.Type_ClassAtt)
                            {
                                _classAttributes.Add(GetClassAttFromXmlData());
                            }
                            else if (type == _globals.Type_ClassRel)
                            {
                                _classRelations.Add(GetClassRelFromXmlData());
                            }
                            else if (type == _globals.Type_ObjectAtt)
                            {
                                var item = GetObjAttFromXmlData();
                                if (item != null)
                                {
                                    _objAtttributes.Add(item);
                                }
                            }
                            else if (type == _globals.Type_ObjectRel)
                            {
                                _objRelations.Add(GetObjRelFromXmlData());
                            }
                        }
                    }
                }
            }
            
            return result;
        }

        private clsOntologyItem GetSimpleItemFromXmlData(string itemType)
        {
            var id = _xmlTextReader.GetAttribute(XMLEntries.Id);
            var idParent = _xmlTextReader.GetAttribute(XMLEntries.Id_Parent);
            _xmlTextReader.Read();

            var name = "";
            name = _xmlTextReader.Value;
            
            
            var oItem_Item = new clsOntologyItem
            {
                GUID = id,
                Name = name,
                GUID_Parent = string.IsNullOrEmpty(idParent) ? null :  idParent,
                Type = itemType
            };

            return oItem_Item;
        }

        private clsClassAtt GetClassAttFromXmlData()
        {
            var idAttributeType = _xmlTextReader.GetAttribute(XMLEntries.Id_AttributeType);
            var idClass = _xmlTextReader.GetAttribute(XMLEntries.Id_Class);
            var idDataType = _xmlTextReader.GetAttribute(XMLEntries.Id_DataType);
            var min = int.Parse(_xmlTextReader.GetAttribute(XMLEntries.Min) ?? "0");
            var max = int.Parse(_xmlTextReader.GetAttribute(XMLEntries.Max) ?? "-1");

            _xmlTextReader.Read();

            var oItem_Item = new clsClassAtt
            {
                ID_AttributeType = idAttributeType,
                ID_Class = idClass,
                ID_DataType = idDataType,
                Min = min,
                Max = max
            };

            return oItem_Item;
        }

        private clsObjectAtt GetObjAttFromXmlData()
        {
            long? orderId;
            string valNamed = null ;
            bool? valBool = null;
            DateTime? valDate = null;
            double? valDouble = null;
            long? valLong = null;
            string valString = null;
            bool hasContent = false;

            var idAttribute = _xmlTextReader.GetAttribute(XMLEntries.Id_Attribute);
            var idAttributeType = _xmlTextReader.GetAttribute(XMLEntries.Id_AttributeType);
            var idDataType = _xmlTextReader.GetAttribute(XMLEntries.Id_DataType);
            var idObject = _xmlTextReader.GetAttribute(XMLEntries.Id_Object);
            var idClass = _xmlTextReader.GetAttribute(XMLEntries.Id_Class);
            var orderIdString = _xmlTextReader.GetAttribute(XMLEntries.OrderId);

            long orderIdTest;
            if (long.TryParse(orderIdString, out orderIdTest))
            {
                orderId = orderIdTest;
            }
            else
            {
                orderId = null;
            }

            if (_xmlTextReader.ReadToDescendant(XMLEntries.Val_Named))
            {
                try
                {
                    valNamed = System.Web.HttpUtility.HtmlDecode(_xmlTextReader.ReadElementContentAsString());
                    if (string.IsNullOrEmpty(valNamed))
                    {
                        valNamed = null;
                    }
                }
                catch (Exception)
                {
                    valNamed = null;
                }
            }
            if (_xmlTextReader.ReadToFollowing(XMLEntries.Val_Bit))
            {
                var value = _xmlTextReader.ReadElementContentAsString();
                bool valTyped;
                if (!string.IsNullOrEmpty(value))
                {
                    if (bool.TryParse(value, out valTyped))
                    {
                        valBool = valTyped;
                        hasContent = true;
                    }
                    
                }
                else
                {
                    hasContent = false;
                }
            }
            if (_xmlTextReader.ReadToFollowing(XMLEntries.Val_Date) && !hasContent)
            {
                var value = _xmlTextReader.ReadElementContentAsString();
                DateTime valTyped;
                if (!string.IsNullOrEmpty(value))
                {
                    if (DateTime.TryParse(value, out valTyped))
                    {
                        valDate = valTyped;
                        hasContent = true;
                    }
                    
                }
                else
                {
                    hasContent = false;
                }
            }
            else if (_xmlTextReader.ReadToFollowing(XMLEntries.Val_Double) && !hasContent)
            {
                var value = _xmlTextReader.ReadElementContentAsString();
                double valTyped;
                if (!string.IsNullOrEmpty(value))
                {
                    if (double.TryParse(value, out valTyped))
                    {
                        valDouble = valTyped;
                        hasContent = true;
                    }
                    
                }
                else
                {
                    hasContent = false;
                }
            }
            if (_xmlTextReader.ReadToFollowing(XMLEntries.Val_Lng) && !hasContent)
            {
                var value = _xmlTextReader.ReadElementContentAsString();
                long valTyped;
                if (!string.IsNullOrEmpty(value))
                {
                    if (long.TryParse(value, out valTyped))
                    {
                        valLong = valTyped;
                        hasContent = true;
                    }
                    
                }
                else
                {
                    hasContent = false;
                }
            }
            if (_xmlTextReader.ReadToFollowing(XMLEntries.Val_String) && !hasContent)
            {
                valString = System.Web.HttpUtility.HtmlDecode(_xmlTextReader.ReadElementContentAsString());
                if (string.IsNullOrEmpty(valString))
                {
                    valString = null;
                    hasContent = false;
                }
                else
                {
                    hasContent = true;
                }
            }
            

            if (hasContent)
            {
                return new clsObjectAtt
                {
                    ID_Attribute = idAttribute,
                    ID_AttributeType = idAttributeType,
                    ID_DataType = idDataType,
                    ID_Class = idClass,
                    ID_Object = idObject,
                    OrderID = orderId,
                    Val_Name = valNamed,
                    Val_Bit = valBool,
                    Val_Date = valDate,
                    Val_Double = valDouble,
                    Val_Lng = valLong,
                    Val_String = valString
                };
            }
            else
            {
                return null;
            }
                                
                               
        }

        private clsObjectRel GetObjRelFromXmlData()
        {
            var idObject = _xmlTextReader.GetAttribute(XMLEntries.Id_Object);
            var idParentObject = _xmlTextReader.GetAttribute(XMLEntries.Id_Parent_Object);
            var idOther = _xmlTextReader.GetAttribute(XMLEntries.Id_Other);
            var idParentOther = _xmlTextReader.GetAttribute(XMLEntries.Id_Parent_Other);
            var idRelationType = _xmlTextReader.GetAttribute(XMLEntries.Id_RelationType);
            var orderIdString = _xmlTextReader.GetAttribute(XMLEntries.OrderId);
            var ontology = _xmlTextReader.GetAttribute(XMLEntries.Ontology);

            long orderIdTest;
            long? orderId = long.TryParse(orderIdString, out orderIdTest) ? orderIdTest : 0;

            return new clsObjectRel
            {
                ID_Object = idObject,
                ID_Parent_Object = idParentObject,
                ID_Other = idOther,
                ID_Parent_Other = idParentOther,
                ID_RelationType = idRelationType,
                OrderID = orderId,
                Ontology = ontology
            };
        }

        private clsClassRel GetClassRelFromXmlData()
        {
            var idClassLeft = _xmlTextReader.GetAttribute(XMLEntries.Id_Class_Left);
            var idClassRight = _xmlTextReader.GetAttribute(XMLEntries.Id_Class_Right);
            var idRelationType = _xmlTextReader.GetAttribute(XMLEntries.Id_RelationType);
            long longTest;
            long? minForw;
            var minForwString = _xmlTextReader.GetAttribute(XMLEntries.Min_Forw);
            minForw = long.TryParse(minForwString, out longTest) ? longTest : 0;
            long? maxForw;
            var maxForwString = _xmlTextReader.GetAttribute(XMLEntries.Max_Forw);
            maxForw = long.TryParse(maxForwString, out longTest) ? longTest : -1;
            long? maxBackw;
            var maxBackwString = _xmlTextReader.GetAttribute(XMLEntries.Max_Backw);
            maxBackw = long.TryParse(maxBackwString, out longTest) ? longTest : -1;
            var ontology = _xmlTextReader.GetAttribute(XMLEntries.Ontology);

            return new clsClassRel
            {
                ID_Class_Left = idClassLeft,
                ID_Class_Right = idClassRight,
                ID_RelationType = idRelationType,
                Min_Forw = minForw,
                Max_Forw = maxForw,
                Max_Backw = maxBackw,
                Ontology = ontology
            };
        }

        private clsOntologyItem FileItems(List<clsOntologyItem> classes, clsOntologyItem classItem = null)
        {
            var classesForImport =
                classes.Where(cls => classItem == null ? string.IsNullOrEmpty(cls.GUID_Parent)  : cls.GUID_Parent == classItem.GUID).ToList();

            var result = classItem != null
                ? _ontologyModDbConnector.SaveClass(new List<clsOntologyItem> {classItem})
                : _globals.LState_Success.Clone();

            if (result.GUID == _globals.LState_Success.GUID)
            {
                foreach (var subClass in classesForImport)
                {
                    result = FileItems(classes, subClass);
                    if (result.GUID == _globals.LState_Error.GUID)
                    {
                        break;
                    }
                }
            }

            return result;
        }

        public ImportWorker(Globals globals)
        {
            _globals = globals;
            Initialize();
        }

        private void Initialize()
        {
            _ontologyModDbConnector = new OntologyModDBConnector(_globals);
        }
    }
}
