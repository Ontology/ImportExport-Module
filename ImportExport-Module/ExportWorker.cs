﻿using ImportExport_Module.Base;
using Newtonsoft.Json;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ImportExport_Module
{
    public enum XMLTemplateType
    {
        ItemContainer = 0,
        ClassAtt = 1,
        ClassRel = 2,
        ObjectAtt = 3,
        ObjectRel = 4,
        OntologyItem = 5
    }

    [Flags]
    public enum ModeEnum
    {
        NoRelations = 0,
        OntologyItems = 1,
        OntologyJoins = 2,
        ClassParents = 4,
        AllRelations = 8,
        OntologyStructures = 16
    }

    public class ExportWorker : NotifyPropertyChange
    {

        private clsLogStates logStates = new clsLogStates();
        private object exportLocker = new object();

        public string Notify_ResultExportItems
        {
            get { return "ResultExportItems"; }
        }

        private clsOntologyItem resultExportItems;
        public clsOntologyItem ResultExportItems
        {
            get { return resultExportItems; }
            set
            {
                resultExportItems = value;
                RaisePropertyChanged(Notify_ResultExportItems);
            }
        }

        private clsOntologyItem resultExportHierarchyToJson;
        public clsOntologyItem ResultExportHierarchyToJson
        {
            get { return resultExportHierarchyToJson; }
            set
            {
                resultExportHierarchyToJson = value;
                RaisePropertyChanged(nameof(ResultExportHierarchyToJson));
            }
        }

        private Thread exportItemsAsync;

        public void StopExport()
        {
            StopExportItems();
        }

        private void StopExportItems()
        {
            if (exportItemsAsync != null)
            {
                try
                {
                    exportItemsAsync.Abort();
                }
                catch(Exception ex) { }
            }
        }

        public clsOntologyItem ExportItems(List<clsOntologyItem> classes,
            List<clsOntologyItem> attributeTypes,
            List<clsOntologyItem> relationTypes,
            List<clsOntologyItem> objects,
            List<clsClassAtt> classAtts,
            List<clsClassRel> classRels,
            List<clsObjectAtt> objectAtts,
            List<clsObjectRel> objectRels)
        {

            StopExportItems();

            exportItemsAsync = new Thread(ExportOntologyAsync);
            


            return logStates.LogState_Success.Clone();
        }

        private void ExportOntologyAsync(object item)
        {
            

            var dictXmlTemplates = GetTemplateXMLs();

            if (dictXmlTemplates == null)
            {
                ResultExportItems = logStates.LogState_Error.Clone();
                return;
            }

            if (!dictXmlTemplates.ContainsKey(XMLTemplateType.ItemContainer) ||
                !dictXmlTemplates.ContainsKey(XMLTemplateType.ClassAtt) ||
                !dictXmlTemplates.ContainsKey(XMLTemplateType.ClassRel) ||
                !dictXmlTemplates.ContainsKey(XMLTemplateType.ObjectAtt) ||
                !dictXmlTemplates.ContainsKey(XMLTemplateType.ObjectRel) ||
                !dictXmlTemplates.ContainsKey(XMLTemplateType.OntologyItem))
            {
                ResultExportItems = logStates.LogState_Error.Clone();
                return;
            }


            var result = logStates.LogState_Success.Clone();

            ResultExportItems = result;
            
        }

        public async Task<clsOntologyItem> ExportHierarchyOntologyToJson(Globals globals, HierarchyParam hierarchyParam, StreamWriter streamWriter)
        {
            var result = globals.LState_Success.Clone();

            lock(exportLocker)
            {
                try
                {
                    var dbReader = new OntologyModDBConnector(globals);
                    var idRootObject = hierarchyParam.FilterRootObject != null ? hierarchyParam.FilterRootObject.GUID : null;
                    var searchHierarchy = new List<clsObjectRel>
                    {

                        
                        new clsObjectRel
                        {
                            ID_Object = hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? idRootObject : null,
                            ID_Parent_Object = hierarchyParam.ClassLeft.GUID,
                            ID_Other = hierarchyParam.Direction.GUID == globals.Direction_RightLeft.GUID ? idRootObject : null,
                            ID_Parent_Other = hierarchyParam.ClassRight.GUID,
                            ID_RelationType = hierarchyParam.RelationType.GUID

                        }
                    };

                    result = dbReader.GetDataObjectRel(searchHierarchy);

                    if (result.GUID == globals.LState_Success.GUID)
                    {
                        List<clsOntologyItem> rootItems = new List<clsOntologyItem>();

                        if (hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID)
                        {
                            rootItems = (from relItem in dbReader.ObjectRels
                                         join parentItem in dbReader.ObjectRels on relItem.ID_Object equals parentItem.ID_Other into parentItems
                                         from parentItem in parentItems.DefaultIfEmpty()
                                         where parentItem == null
                                         select relItem).GroupBy(relItm => new {GUID = relItm.ID_Object, Name = relItm.Name_Object, GUID_Parent = relItm.ID_Parent_Object})
                                         .Select(relItm => new clsOntologyItem
                                         {
                                             GUID = relItm.Key.GUID,
                                             Name = relItm.Key.Name,
                                             GUID_Parent = relItm.Key.GUID_Parent,
                                             Type = globals.Type_Object
                                         }).ToList();

                        }
                        else
                        {
                            rootItems = (from relItem in dbReader.ObjectRels
                                         join parentItem in dbReader.ObjectRels on relItem.ID_Other equals parentItem.ID_Object into parentItems
                                         from parentItem in parentItems.DefaultIfEmpty()
                                         where parentItem == null
                                         select relItem).GroupBy(relItm => new { GUID = relItm.ID_Other, Name = relItm.Name_Other, GUID_Parent = relItm.ID_Parent_Other })
                                         .Select(relItm => new clsOntologyItem
                                         {
                                             GUID = relItm.Key.GUID,
                                             Name = relItm.Key.Name,
                                             GUID_Parent = relItm.Key.GUID_Parent,
                                             Type = globals.Type_Object
                                         }).ToList();
                        }

                        using (streamWriter)
                        {
                            using (var jsonTextWriter = new JsonTextWriter(streamWriter))
                            {
                                jsonTextWriter.WriteStartArray();

                                foreach(var rootItem in rootItems)
                                {
                                    jsonTextWriter.WriteStartObject();
                                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_Id) ? hierarchyParam.ParamName_Id : nameof(clsOntologyItem.GUID));
                                    jsonTextWriter.WriteValue(rootItem.GUID);

                                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_Name) ? hierarchyParam.ParamName_Name : nameof(clsOntologyItem.Name));
                                    jsonTextWriter.WriteValue(rootItem.Name);

                                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_IdClass) ? hierarchyParam.ParamName_IdClass : nameof(clsOntologyItem.GUID_Parent));
                                    jsonTextWriter.WriteValue(rootItem.GUID_Parent);

                                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_NameClass) ? hierarchyParam.ParamName_NameClass : nameof(clsOntologyItem.Name_Parent));
                                    jsonTextWriter.WriteValue(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? hierarchyParam.ClassLeft.Name : hierarchyParam.ClassRight.Name);

                                    result = WriteSubItems(rootItem.GUID, jsonTextWriter, hierarchyParam, dbReader.ObjectRels, globals);
                                    if (result.GUID == globals.LState_Error.GUID)
                                    {
                                        break;
                                    }

                                    jsonTextWriter.WriteEndObject();

                                }
                                jsonTextWriter.WriteEndArray();

                            }
                        }
                    }

                    
                }
                catch (Exception ex)
                {
                    result = logStates.LogState_Error.Clone();
                }
                
            }

            ResultExportHierarchyToJson = result;
            return result;
        }

        private clsOntologyItem WriteSubItems(string parentId, JsonTextWriter jsonTextWriter, HierarchyParam hierarchyParam, List<clsObjectRel> relationItems, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            var subItems = relationItems.Where(relItm => hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? relItm.ID_Object == parentId : relItm.ID_Other == parentId).ToList();

            if (subItems.Any())
            {
                jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_SubItems) ? hierarchyParam.ParamName_SubItems : "SubItems");
                jsonTextWriter.WriteStartArray();
            }

            foreach (var subItem in subItems)
            {
                try
                {
                    jsonTextWriter.WriteStartObject();
                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_Id) ? hierarchyParam.ParamName_Id : nameof(clsOntologyItem.GUID));
                    jsonTextWriter.WriteValue(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? subItem.ID_Other : subItem.ID_Object);

                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_Name) ? hierarchyParam.ParamName_Name : nameof(clsOntologyItem.Name));
                    jsonTextWriter.WriteValue(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? subItem.Name_Other : subItem.Name_Object);

                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_IdClass) ? hierarchyParam.ParamName_IdClass : nameof(clsOntologyItem.GUID_Parent));
                    jsonTextWriter.WriteValue(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? subItem.ID_Parent_Other : subItem.ID_Parent_Object);

                    jsonTextWriter.WritePropertyName(!string.IsNullOrEmpty(hierarchyParam.ParamName_NameClass) ? hierarchyParam.ParamName_NameClass : nameof(clsOntologyItem.Name_Parent));
                    jsonTextWriter.WriteValue(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? hierarchyParam.ClassLeft.Name : hierarchyParam.ClassRight.Name);

                    result = WriteSubItems(hierarchyParam.Direction.GUID == globals.Direction_LeftRight.GUID ? subItem.ID_Other : subItem.ID_Object, jsonTextWriter, hierarchyParam, relationItems, globals);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        break;
                    }
                    jsonTextWriter.WriteEndObject();
                }
                catch (Exception)
                {
                    result = logStates.LogState_Error.Clone();

                }
            }

            if (subItems.Any())
            {
                jsonTextWriter.WriteEndArray();
            }
            return result;
        }

        private Dictionary<XMLTemplateType, string> GetTemplateXMLs()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var dictXMLTemplates = new Dictionary<XMLTemplateType, string>();
            try
            {
                var xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.OntologyItemContainerTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.ItemContainer, xmlStreamReader.ReadToEnd());
                }

                xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.ClassAttItemTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.ClassAtt, xmlStreamReader.ReadToEnd());
                }

                xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.ClassRelItemTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.ClassRel, xmlStreamReader.ReadToEnd());
                }

                xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.ObjAttItemTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.ObjectAtt, xmlStreamReader.ReadToEnd());
                }

                xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.ObjRelItemTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.ObjectRel, xmlStreamReader.ReadToEnd());
                }

                xmlStream = assembly.GetManifestResourceStream("ImportExport_Module.OntologyItemTemplate.xml");
                using (var xmlStreamReader = new StreamReader(xmlStream, true))
                {
                    dictXMLTemplates.Add(XMLTemplateType.OntologyItem, xmlStreamReader.ReadToEnd());
                }
                return dictXMLTemplates;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }

    public class HierarchyParam : NotifyPropertyChange
    {
        private clsOntologyItem classLeft;
        public clsOntologyItem ClassLeft
        {
            get { return classLeft; }
            set
            {
                classLeft = value;
                RaisePropertyChanged(nameof(ClassLeft));
            }
        }

        private clsOntologyItem relationType;
        public clsOntologyItem RelationType
        {
            get { return relationType; }
            set
            {
                relationType = value;
                RaisePropertyChanged(nameof(RelationType));
            }
        }

        private clsOntologyItem classRight;
        public clsOntologyItem ClassRight
        {
            get { return classRight; }
            set
            {
                classRight = value;
                RaisePropertyChanged(nameof(ClassRight));
            }
        }

        private clsOntologyItem direction;
        public clsOntologyItem Direction
        {
            get { return direction; }
            set
            {
                direction = value;
                RaisePropertyChanged(nameof(Direction));
            }
        }

        private clsOntologyItem filterRootObject;
        public clsOntologyItem FilterRootObject
        {
            get { return filterRootObject; }
            set
            {
                filterRootObject = value;
                RaisePropertyChanged(nameof(FilterRootObject));
            }
        }

        private string paramName_Id;
        public string ParamName_Id
        {
            get { return paramName_Id; }
            set
            {
                paramName_Id = value;
                RaisePropertyChanged(nameof(ParamName_Id));
            }
        }

        private string paramName_Name;
        public string ParamName_Name
        {
            get { return paramName_Name; }
            set
            {
                paramName_Name = value;
                RaisePropertyChanged(nameof(ParamName_Name));
            }
        }

        private string paramName_IdClass;
        public string ParamName_IdClass
        {
            get { return paramName_IdClass; }
            set
            {
                paramName_IdClass = value;
                RaisePropertyChanged(nameof(ParamName_IdClass));
            }
        }

        private string paramName_NameClass;
        public string ParamName_NameClass
        {
            get { return paramName_NameClass; }
            set
            {
                paramName_NameClass = value;
                RaisePropertyChanged(nameof(ParamName_NameClass));
            }
        }

        private string paramName_SubItems;
        public string ParamName_SubItems
        {
            get { return paramName_SubItems; }
            set
            {
                paramName_SubItems = value;
                RaisePropertyChanged(nameof(ParamName_SubItems));
            }
        }

        public bool IsValid
        {
            get; private set;
        }

        public HierarchyParam()
        {
            PropertyChanged += HierarchyParam_PropertyChanged;
        }

        private void HierarchyParam_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (ClassLeft == null || ClassRight == null || RelationType == null)
            {
                IsValid = false;
                return;
            }

            if (!string.IsNullOrEmpty(ParamName_Id))
            {
                if (ParamName_Id == ParamName_Name ||
                    ParamName_Id == ParamName_IdClass ||
                    ParamName_Id == ParamName_NameClass ||
                    ParamName_Id == ParamName_SubItems)
                {
                    IsValid = false;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(ParamName_Name))
            {
                if (ParamName_Name == ParamName_Id ||
                    ParamName_Name == ParamName_IdClass ||
                    ParamName_Name == ParamName_NameClass ||
                    ParamName_Name == ParamName_SubItems)
                {
                    IsValid = false;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(ParamName_IdClass))
            {
                if (ParamName_IdClass == ParamName_Id ||
                    ParamName_IdClass == ParamName_Name ||
                    ParamName_IdClass == ParamName_NameClass ||
                    ParamName_IdClass == ParamName_SubItems)
                {
                    IsValid = false;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(ParamName_NameClass))
            {
                if (ParamName_NameClass == ParamName_Id ||
                    ParamName_NameClass == ParamName_Name ||
                    ParamName_NameClass == ParamName_IdClass ||
                    ParamName_NameClass == ParamName_SubItems)
                {
                    IsValid = false;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(ParamName_SubItems))
            {
                if (ParamName_SubItems == ParamName_Id ||
                    ParamName_SubItems == ParamName_Name ||
                    ParamName_SubItems == ParamName_IdClass ||
                    ParamName_SubItems == ParamName_NameClass)
                {
                    IsValid = false;
                    return;
                }
            }


            IsValid = true;
        }
    }

}
